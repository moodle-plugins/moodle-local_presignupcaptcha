<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Callbacks used by Moodle API.
 * @package    local_presignupcaptcha
 * @copyright  2023 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Redirect users that want to signup to the captcha page, if they have not passed the captcha test yet.
 */
function local_presignupcaptcha_pre_signup_requests() {
    global $CFG, $PAGE, $OUTPUT, $SITE;
    if (get_config('local_presignupcaptcha', 'enabled') == 1
            && (cache::make('core', 'presignup')->get('local_presignupcaptcha_captchapassed') ?? 0) < time() - 3 * 3600) {

        // Pre-signup, it can happen that the session resets (and I have no idea why).
        // The following workaround will keep previously entered data on the signup form
        // and allow it to be restored after captcha is passed again.
        if (optional_param('sesskey', null, PARAM_RAW) !== null && !confirm_sesskey()) {
            // Sesskey was invalidated, add it to post and retry so we don't lose form data.
            // It is fine to do that as this newly generated sesskey doesn't allow much.
            $_POST['sesskey'] = sesskey();
            $sessionresetmessage = $OUTPUT->notification(get_string('sessionresetmessage', 'local_presignupcaptcha'), 'info');
        } else {
            $sessionresetmessage = null;
        }
        $signupform = get_auth_plugin($CFG->registerauth)->signup_form(); // We know this exists when this callback is called.
        $formdata = $signupform->is_cancelled() ? null : $signupform->get_submitted_data();
        if ($signupform != null && $signupform->is_submitted()) {
            // Set the _qf__ flag again, so the form is re-submitted after the captcha
            // (so users do not have to click the submit button again).
            $formdata->{'_qf__' . preg_replace('/[^a-z0-9_]/i', '_', get_class($signupform))} = 1;
        }

        $form = new local_presignupcaptcha\captcha_form(null, $formdata);

        if ($form->is_cancelled()) {
            // Cancelled captcha form: redirect to login page.
            redirect(get_login_url());
        } else if ($form->is_submitted() && $form->is_validated()) {
            // Captcha is correct: set a flag for the current session and continue to the signup page.
            cache::make('core', 'presignup')->set('local_presignupcaptcha_captchapassed', time());
            return;
        } else {
            // Display the form.
            $PAGE->set_pagelayout('login');
            echo $OUTPUT->header();
            echo $OUTPUT->render_from_template('local_presignupcaptcha/form', array(
                    'logourl' => $OUTPUT->get_logo_url(),
                    'sitename' => format_string($SITE->fullname, true),
                    'formhtml' => $form->render(),
                    'notification' => $sessionresetmessage
            ));
            die(); // Do not continue to the signup page!
        }
    }
}
