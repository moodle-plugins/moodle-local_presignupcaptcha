<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_presignupcaptcha', language 'fr'
 * @package    local_presignupcaptcha
 * @copyright  2023 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Captcha pré-inscription';
$string['privacy:metadata'] = 'Le plugin Captcha pré-inscription ne stocke aucune donnée personnelle.';

$string['captchaprompt'] = 'Afin de prouver que vous n\'êtes pas un robot, veuillez recopier le texte que vous voyez sur l\'image';
$string['errorcaptchainvalid'] = 'Réponse invalide, veuillez réessayer.';
$string['errorcaptchatimeout'] = 'Une erreur s\'est produite. Le Captcha a probalement expiré, veuillez réessayer.';
$string['jsrequired'] = 'Ce Captcha nécessite que JavaScript soit activé pour s\'afficher.';
$string['refreshcaptcha'] = 'Rafraîchir le Captcha';
$string['sessionresetmessage'] = 'Il semble que votre session ait été réinitialisée. Veuillez compléter à nouveau le Captcha.';
$string['settings:enabled'] = 'Activer le Captcha pré-inscription';
$string['settings:enabled_desc'] = 'Si réglé sur "Oui", les nouveaux utilisateurs essayant de s\'inscrire devront d\'abord compléter un Captcha textuel à sécurité moyenne.';
