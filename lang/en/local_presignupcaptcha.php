<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_presignupcaptcha', language 'en'
 * @package    local_presignupcaptcha
 * @copyright  2023 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Presignup Captcha';
$string['privacy:metadata'] = 'The Presignup Captcha plugin does not store any personal data.';

$string['captchaprompt'] = 'To prove you are not a robot, please enter the text you see on the image';
$string['errorcaptchainvalid'] = 'Invalid answer, please try again.';
$string['errorcaptchatimeout'] = 'An error has occured. The captcha has most likely timed out, please try again.';
$string['jsrequired'] = 'This Captcha requires JavaScript to be enabled.';
$string['refreshcaptcha'] = 'Refresh Captcha';
$string['sessionresetmessage'] = 'It seems that the session was reset (sorry about that). Please re-enter the captcha.';
$string['settings:enabled'] = 'Enable pre-signup Captcha';
$string['settings:enabled_desc'] = 'If set to "Yes", new users trying to signup will be first prompted with a text captcha with moderate security.';
