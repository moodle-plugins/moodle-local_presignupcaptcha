<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A pre-signup form with a single text captcha, with moderate security.
 * @package    local_presignupcaptcha
 * @copyright  2023 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_presignupcaptcha;
use moodleform, MoodleQuickForm;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/formslib.php');

/**
 * Text captcha form definition.
 * @copyright  2023 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class captcha_form extends moodleform {
    /**
     * {@inheritDoc}
     * @see moodleform::definition()
     */
    protected function definition() {
        global $CFG;

        $mform = $this->_form;

        if (!empty($this->_customdata)) {
            foreach ($this->_customdata as $key => $value) {
                if (preg_match('#^(submitbutton$|cancel$)#', $key)) {
                    continue;
                }
                $mform->addElement('hidden', $key, $value);
                $mform->setType($key, PARAM_RAW);
            }
        }

        if (!$mform->isTypeRegistered('simplecaptcha')) {
            MoodleQuickForm::registerElementType('simplecaptcha',
                    "$CFG->dirroot/local/presignupcaptcha/classes/simplecaptcha.php", 'local_presignupcaptcha\simplecaptcha');
        }

        $mform->addElement('simplecaptcha', 'captcha', get_string('captchaprompt', 'local_presignupcaptcha'));

        $this->add_action_buttons(true, get_string('continue'));
    }

    /**
     * {@inheritDoc}
     * @see moodleform::validation()
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // Verify that the captcha is correct.
        $captchaelement = $this->_form->getElement('captcha');
        if (empty($data['captcha']) || ($message = $captchaelement->verify($data['captcha'])) !== true) {
            $errors['captcha'] = $message;
        }

        return $errors;
    }
}
