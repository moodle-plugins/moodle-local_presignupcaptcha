<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Form element (for a MoodleQuickForm) to display a simple text captcha with moderate security.
 * Validity of the response should be checked in form validation() method by calling this class' verify() method.
 * @package    local_presignupcaptcha
 * @copyright  2023 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_presignupcaptcha;
use MoodleQuickForm_group, HTML_QuickForm_group;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/form/group.php');
require_once(__DIR__ . '/../locallib.php');

/**
 * Simple text captcha element for MoodleQuickForm definition
 * @copyright  2023 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class simplecaptcha extends MoodleQuickForm_group {

    /**
     * @var string The captcha text (6 characters).
     */
    private $text;
    /**
     * @var string The hash of captcha text combined with a generated private key.
     */
    private $hash;

    /**
     * Constructor
     *
     * @param string $elementname (optional) name of the simplecaptcha element
     * @param string $elementlabel (optional) label for simplecaptcha element
     * @param mixed $attributes (optional) Either a typical HTML attribute string or an associative array
     */
    public function __construct($elementname = null, $elementlabel = null, $attributes = null) {
        parent::__construct($elementname, $elementlabel, $attributes);
        $this->_persistantFreeze = true;
        $this->_appendName = true;

        list($this->text, $this->hash) = local_presignupcaptcha_generate();
    }

    /**
     * Old syntax of class constructor. Deprecated in PHP7.
     *
     * @deprecated since Moodle 3.1
     */
    public function local_presignupcaptcha_simplecaptcha($elementname = null, $elementlabel = null,  $attributes = null) {
        debugging('Use of class name as constructor is deprecated', DEBUG_DEVELOPER);
        self::__construct($elementname, $elementlabel, $attributes);
    }

    /**
     * {@inheritDoc}
     * @see HTML_QuickForm_group::_createElementsIfNotExist()
     */
    public function _createElementsIfNotExist() {
        // We want to recreate the elements even if they exist, to have a proper reload of the image and the fields.
        $this->_createElements();
    }

    /**
     * {@inheritDoc}
     * @see HTML_QuickForm_group::_createElements()
     */
    public function _createElements() {
        global $CFG;
        $this->_elements = array();
        // Create a canvas and a script that will write on it.
        // We do this from inline javascript because including AMD modules from here is troublesome.
        $this->_elements['captchaimage'] = $this->createFormElement('html',
                '<canvas id="simpleCaptcha" width="200" height="35" style="background-color:cornsilk"></canvas>
                <i id="regenerateCaptcha" class="fa fa-refresh clickable mx-1"
                    title="' . get_string('refreshcaptcha', 'local_presignupcaptcha') . '"></i>
                <script id="captchascript">
                    document.getElementById("captchascript").remove();
                    var setupCaptcha = function(text, hash) {
                        if (typeof hash != "undefined") {
                            document.getElementsByName("' . $this->_name . '[captchahash]")[0].value = hash;
                        }
                        var c = document.getElementById("simpleCaptcha");
                        var ctx = c.getContext("2d");
                        ctx.clearRect(0, 0, c.width, c.height);
                        ctx.font = "25px Arial";
                        ctx.fillText(atob(text) , (c.width/2) - 65, 25);
                        ctx.beginPath();
                        if (Math.random()<0.5) {
                            ctx.moveTo(Math.random()*40+10, Math.random()*10+5);
                            ctx.bezierCurveTo(80, Math.random()*10+30,
                                              120, Math.random()*10-5,
                                              Math.random()*40+150, Math.random()*10+20);
                        } else {
                            ctx.moveTo(Math.random()*40+10, Math.random()*10+20);
                            ctx.bezierCurveTo(80, Math.random()*10-5,
                                              120, Math.random()*10+30,
                                              Math.random()*40+150, Math.random()*10+5);
                        }
                        ctx.stroke();
                        ctx.beginPath();
                        ctx.moveTo(Math.random()*150+25, 5);
                        ctx.lineTo(Math.random()*150+25, 30);
                        ctx.stroke();
                    };
                    setupCaptcha("' . base64_encode(implode(' ', str_split($this->text))) . '");
                    document.getElementById("regenerateCaptcha").onclick = function() {
                        fetch("' . $CFG->wwwroot . '/local/presignupcaptcha/ajax/regenerate_captcha.php")
                        .then(response => response.json())
                        .then(response => {
                            if (response.success) {
                                setupCaptcha(response.response.text, response.response.hash);
                            }
                        });
                    };
                </script>
                <noscript><div class="text-danger">' . get_string('jsrequired', 'local_presignupcaptcha') . '</div></noscript>');
        // Create the main text input.
        $textinput = $this->createFormElement('text', 'captchainput', null, array('size' => 25, 'maxlength' => 11));
        $textinput->set_force_ltr(true);
        $this->_mform->setType($this->_name . '[captchainput]', PARAM_ALPHANUM);
        $this->_elements['captchainput'] = $textinput;

        // Create a hidden element with the hash for validation.
        $hiddeninput = $this->createFormElement('hidden', 'captchahash', $this->hash);
        $this->_mform->setType($this->_name . '[captchahash]', PARAM_RAW);
        $this->_elements['captchahash'] = $hiddeninput;
    }

    /**
     * Validate the response
     * @param array $response
     * @return string|bool true if the response is valid, otherwise a message explaining that it is invalid
     */
    public function verify($response) {
        $cache = local_presignupcaptcha_getkeycache();
        if (empty($cache->get('privatekey')) || $cache->get('gentime') < time() - 6 * 3600) {
            return get_string('errorcaptchatimeout', 'local_presignupcaptcha');
        }
        $responsehash = hash('sha256', trim(str_replace(' ', '', $response['captchainput'])) . $cache->get('privatekey'));
        return $responsehash == $response['captchahash'] ? true : get_string('errorcaptchainvalid', 'local_presignupcaptcha');
    }

}
