<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local lib for local_presignupcaptcha.
 * @package    local_presignupcaptcha
 * @copyright  2023 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Generate an alphanumeric text captcha.
 * @return string[] of two elements: expected text and corresponding hash
 */
function local_presignupcaptcha_generate() {
    // Include all alphanumeric characters, except the ones that might be confusing.
    $charset = array ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',      'J', 'K', 'L', 'M',
                      'N',      'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',      'm',
                      'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                      '2', '3', '4', '5', '6', '7', '8', '9');
    $text = '';
    for ($i = 0; $i < 6; $i++) {
        $text .= $charset[rand(0, count($charset) - 1)];
    }

    $cache = local_presignupcaptcha_getkeycache();

    if (empty($cache->get('privatekey')) || $cache->get('gentime') < time() - 3 * 3600) {
        $cache->set('privatekey', uniqid());
        $cache->set('gentime', time());
    }
    $hash = hash('sha256', $text . $cache->get('privatekey'));
    return array($text, $hash);
}

/**
 * Get the session cache used for the captcha private key.
 * @return cache_session
 */
function local_presignupcaptcha_getkeycache() {
    return cache::make_from_params(cache_store::MODE_SESSION, 'local_presignupcaptcha', 'privatekeycache',
            array(), array('simplekeys' => true, 'simpledata' => true));
}
